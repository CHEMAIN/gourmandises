<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>GOURMANDISES</title>
        <meta name="viewport content="with="device-content, initial-scale=1">
        <link rel="stylesheet" href="style.css">

    </head>
    <body>
        <header>
            <div id="logo-nav"><a href="index.html"><img src="img/gourmandise-logo-icone.svg" alt="icone logo Gourmandise"></a>
            </div>
            <nav>
                <ul>
                    <li> <a href="Une_histoire_de_famille.html">UNE HISTOIRE DE FAMILLE</a></li>
                    <li> <a href="Notre_savoir_faire.html">NOTRE SAVOIR-FAIRE</a></li>
                    <li> <a href="Nos_gourmandises.html">NOS GOURMANDISES</a> </li>
                    <li> <a href="Nous_contacter.html">NOUS CONTACTER</a></li>
                </ul>
            </nav>
                <h1 id="Les-ateliers">Les Ateliers</h1>
            <div id="logo">
                <img src="img/gourmandise-logo.svg" alt="logo Gourmandises">
            </div>
                <h1 id="depuis">depuis 1811</h1>
            <!-- CAROUSSEL -->
            <div id="caroussel">
                <figure>
                    <img src="img/cookies.jpg" alt="cookie"/>
                    <img src="img/boules.jpg" alt="boules_de_chocolat">
                    <img src="img/tablettes.jpg" alt="tablette_de_chocolat">
                    <img src="img/meringues.jpg" alt="meringues_rose">
                    <img src="img/cupcakes.jpg" alt="cupcakes_chocolat">
                </figure>
            </div>
            <!-- FIN CAROUSSEL -->
        </header>

        <main>
            <!-- HISTOIRE -->
            <article id="histoire"">
                <div>
                    <h2> Une Histoire de Famille</h2>
                    <ul><li><a href="Une_histoire_de_famille.html">En savoir +</a></li>
                </div>
                <div>
                    <img src="img/carte-postale.jpg"alt="carte_postale"/>
                </div>
            </article>
            <!-- FIN HISTOIRE -->
            <!-- SAVOIR FAIRE -->
            <article id="savoirFaire"">
                 <div>
                    <h2> Notre Savoir-Faire</h2>
                    <ul><li><a href="Notre_savoir_faire.html">En savoir +</a></li>
                    </ul>
                 </div>
                 <div>
                     <img src="img/carte-postale.jpg"alt="carte_postale"/>
                 </div>
            </article>
            <!-- FIN SAVOIR FAIRE -->
            <!-- ENVIRONNEMENT -->
            <article id="environnement">    
                <h3> Notre démarche Haute Qualité Environnementale</h3>
                <p>Lorem ipsum dolor sit amet. Et voluptatem consequuntur et pariatur necessitatibus est adipisci illo 
                    sed dignissimos eius. Sit tenetur saepe ut rerum eius vel maxime dolorem. Ab harum fugiat hic consequuntur 
                    dignissimos eum praesentium aspernatur.<br><br> 
                    Ea repellat alias est eveniet veniam ab molestias dolor? Cum sunt ipsam et laudantium galisum ut 
                    quidem obcaecati ut velit commodi. Id vero voluptatem sit quod modi ut facilis accusantium qui neque 
                    adipisci? 33 itaque impedit eos voluptate omnis rem consequatur quasi nam reprehenderit commodi et 
                    consequatur totam ea dolore aliquid.<br>
                    Ut fugiat adipisci 33 provident fuga sit quod dolores ut architecto corrupti non dolor laudantium. 
                    Et doloribus internos est debitis totam sit voluptatem consequatur hic consequatur consequatur ut 
                    ipsa consequatur.</p>
            </article>
            <!-- FIN ENVIRONNEMENT -->
        </main>
        <aside>

        </aside>
        <!-- PIED DE PAGE -->
        <footer>
            <nav id="nav-footer">
                <ul>
                    <li> <a href="Une_histoire_de_famille.html">Une histoire de famille</a></li>
                    <li> <a href="Notre_savoir_faire.html">Notre savoir-faire </li>
                    <li> <a href="Nos_gourmandises.html">Nos gourmandises </li>
                    <li> <a href="Nous_contacter.html">Nous contacter </li>
                </ul>
            </nav>
            <nav id="mentions-site">
                <ul>
                    <li> <a href="Mentions_legeles.html">MENTIONS LEGALES</a></li>
                    <li> <a href="Plan_du_site.html">PLAN DU SITE </li>
                </ul>
            </nav>
            <div id="reseaux">
                <p>F</p>
                <p>In</p>
                <p>INTA</p>
            </div>

        </footer>

    </body>
</html>
