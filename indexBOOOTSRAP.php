<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>GOURMANDISES</title>
        <meta name="viewport content="with="device-content, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    </head>
    <body>
        <header class="container-fluid" style="background: grey">
            <nav class="container-fluid" position="top-fixed">
                <h2> NAVIGATION </h2>
            </nav>
            <h1> GOURMANDISE </h1>
        </header>
        <!-- CAROUSSEL -->
        <main class="container-fluid" style="background: appworkspace">
            <h1> CAROUSSEL </h1>
        </main>
        <!-- HISTOIRE et SAVOIR FAIRE -->
        <section class="container-fluid" style="background: grey">
            <div class=row">
                <div id="SavoirFaire" class="col-md-4" style="border:1px solid black">
                    <h1> NOTRE SAVOIRE-FAIRE </h1>
                    <p> Proinde <span class="text-primary">concepta</span> rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    </p>
                </div>
                <div id="Histoire" class="col-md-8" style="border:1px solid black">
                    <h1> NOTRE HISTOIRE </h1>
                    <p> Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    </p>
                </div>
            </div>
        </section>
        <!-- GALERIE -->
        <section class="container-fluid" style="background: appworkspace">
            <h1> GALERIE <small>1</small></h1>
             <h2> GALERIE </h2>
             <h3> GALERIE <mark>important</mark></h3>
             <h4> GALERIE </h4>
             <h5> GALERIE </h5>
             <h6> GALERIE </h6>
        </section>
        <!-- CONTACT -->
        <section class="container-fluid">
            <div class="continer-fluid">
                <div class="row">
                    <div class="col-md-4">
                    <p> Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    </p>
                    </div>
                    <div class="col-md-4">
                    <p> Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili 
                    in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis 
                    sudoribus induratae.
                    </p>
                    </div>
                    <div class="col-md-4">
                    <form style="form">
                    <div class="form-group">
                        <label for="Nom">Nom</label>
                        <input type="text"  id="Nom" class="form-control" required><br>
                        <label for="Prénom">Prénom</label>
                        <input type="text" id="Prenom" class="form-control" required><br>
                        <label for="Adresse">Adresse</label>
                        <input type="text" id="Adresse" class="form-control" required><br>
                        <label for="codePostal">Code postal</label>
                        <input type="text" id="codePostal" class="form-control" required><br>
                        <label for="ville">Ville</label>
                        <input type="text" id="ville" class="form-control" required><br>
                        <label for="commentaires">Votre message</label>
                        <textarea id="commentaires" name="Votre message" rows="5" cols="19" class="form-control">  
                        </textarea><br>
                        <button type="submit" class="btn btn-default">Envoyer</button>
                    </div>
                    </form> 
                    </div>
                </div>
            </form>
             
        </section >
       <!-- PIED DE PAGE -->
        <section class="container-fluid" style="background: appworkspace">
             <h1> PIED </h1>
        </section>
        
    </body>
</html>
